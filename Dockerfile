FROM fedora:25
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
RUN pip3 install ipython
COPY . /app
RUN cd /app && python3 setup.py install && cd -
RUN bash -c " echo -e 'eval \"\$(_INCUBATOR_COMPLETE=source incubator)\" '  >> ~/.bashrc "
RUN rm -rf /app
CMD ["ipython"]
