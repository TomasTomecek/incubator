import io
import logging
import os
import shutil
import tarfile
import tempfile
import unittest

import incubator
from incubator.core.client import DockerClient
from tests.container_testing import (make_test_on_containers,
                                     make_test_on_containers_local)


class ApiTest(unittest.TestCase):
    def setUp(self):
        self.client = DockerClient()

    @classmethod
    def setUpClass(cls):
        (DockerClient()).pull_image("busybox")
        incubator.set_logging(level=logging.DEBUG)

    def tearDown(self):
        pass

    def mk_test_dockerfile(self, directory=None,
                           dockerfile_content=None,
                           context_dir_function=None,
                           ordered=True,
                           **kwargs):
        if directory or context_dir_function:
            incubator_result, orig_result = make_test_on_containers_local(client=self.client,
                                                                          dockerfile_content=dockerfile_content,
                                                                          build_directory=directory,
                                                                          context_tempdir_function=context_dir_function,
                                                                          **kwargs)
        else:
            incubator_result, orig_result = make_test_on_containers(client=self.client,
                                                                    fileobj=io.BytesIO(dockerfile_content.encode()),
                                                                    **kwargs)
        if not ordered:
            incubator_result = set(incubator_result)
            orig_result = set(orig_result)

        self.assertEqual(incubator_result, orig_result)

    def test_build(self):
        self.mk_test_dockerfile(command="id",
                                dockerfile_content="From busybox\n")

    def test_build_local(self):

        def _make_temp_dir_context():
            build_dir = tempfile.mkdtemp()
            with open(os.path.join(build_dir, "Dockerfile"), mode="w") as dockerfile:
                dockerfile.writelines(["FROM busybox\n"])
            with open(os.path.join(build_dir, "file.txt"), mode="w") as file:
                file.writelines(["FILE\n"])
            return build_dir

        self.mk_test_dockerfile(command="id",
                                context_dir_function=_make_temp_dir_context)

    @unittest.skip("not implemented yet")
    def test_env(self):
        """
        Testing ENV.
        """
        for dockerfile_line, test in [("VARIABLE=something", "echo $VARIABLE"),
                                      ("VAR1=something VAR2=other", "echo $VAR1-$VAR2"),
                                      ('myName="John Doe" myDog=Rex\ The\ Dog \\' + '\nmyCat=fluffy',
                                       "echo $myName-$myDog-$myCat"),
                                      ("ENV myName John Doe\nENV myDog Rex The Dog\nENV myCat fluffy",
                                       "echo $myName-$myDog-$myCat")]:
            self.mk_test_dockerfile(dockerfile_content="FROM busybox\nENV {}\n".format(dockerfile_line),
                                    command=test)

    def test_workdir(self):
        for df_content, cmd, layers in [("WORKDIR /tmp\n", "echo $PWD", []),
                                        ("WORKDIR /tmp/not/existing/folder\n", "echo $PWD", []),
                                        ("WORKDIR /tmp\n"
                                         r"RUN touch a", "find /tmp/a", [0]),
                                        ("WORKDIR /tmp\n"
                                         r"ADD Dockerfile ./D", "find /tmp/D", [0]),
                                        ("WORKDIR /a/1\n"
                                         "RUN touch a1.txt\n"
                                         "WORKDIR /a/2/\n"
                                         "RUN touch a2.txt\n"
                                         "WORKDIR ../3/\n"
                                         "RUN touch a3.txt", "find /a", [0])
                                        ]:
            self.mk_test_dockerfile(dockerfile_content="FROM busybox\n{}".format(df_content),
                                    command=cmd,
                                    config={'layers': layers},
                                    ordered=False)

    def test_user(self):
        self.mk_test_dockerfile(dockerfile_content="FROM busybox\nUSER 991\n",
                                command="id -u")

    def test_not_present_image(self):
        self.mk_test_dockerfile(dockerfile_content="FROM alpine:edge",
                                command="id")

    def test_copy_basic(self):
        self._make_copyadd_test_basic(cmd='COPY')

    def test_copy_subdirectory(self):
        self._make_copyadd_test_subdirectory(cmd='COPY')

    def test_copy_dot(self):
        self._make_copyadd_test_dot(cmd='COPY')

    def test_add_basic(self):
        self._make_copyadd_test_basic(cmd='ADD')

    def test_add_subdirectory(self):
        self._make_copyadd_test_subdirectory(cmd='ADD')

    def test_add_dot(self):
        self._make_copyadd_test_dot(cmd='ADD')

    def test_add_archive(self):
        for df, cmd in [("FROM busybox\n"
                         "ADD archive.tar /add/", "find /add")]:
            build_dir = self._get_test_dir_with_archive()
            self.mk_test_dockerfile(dockerfile_content=df,
                                    directory=build_dir,
                                    command=cmd)

    def _make_copyadd_test_basic(self, cmd):
        df = "FROM busybox\n" \
             "{0} D* B* /fldr/0/\n" \
             "{0} file.txt /fldr/1/\n" \
             "{0} f?le.txt /fldr/2/\n" \
             "{0} eee.txt /fldr/3/fff.txt\n".format(cmd)
        self._make_copyadd_test_base(dockerfile_content=df)

    def _make_copyadd_test_subdirectory(self, cmd):
        df = "FROM busybox\n" \
             "{0} DA/a /fldr/4/\n" \
             "{0} DA/a /fldr/5/aaa.txt".format(cmd)
        self._make_copyadd_test_base(dockerfile_content=df)

    def _make_copyadd_test_dot(self, cmd):
        df = "FROM busybox\n" \
             "{0} . /fldr/6/\n" \
             "{0} ./D* /fldr/7/".format(cmd)
        self._make_copyadd_test_base(dockerfile_content=df)

    def _make_copyadd_test_base(self, dockerfile_content):
        build_dir = self._get_test_dir()
        self.mk_test_dockerfile(dockerfile_content=dockerfile_content,
                                directory=build_dir,
                                command="find /fldr",
                                ordered=False)

    @staticmethod
    def _get_test_dir():
        build_dir = tempfile.mkdtemp()
        dirs = ["DA", "DB", "DC", "dir-a", "DA/D", "D F", "D", "D/AD", "B", "B/BB"]
        for d in dirs:
            os.makedirs(os.path.join(build_dir, d))
        for f in ["DA/a", "DA/D/e", "DA/D/c", "DA/D/x", "D/AD/y", "file.txt", "eee.txt"]:
            with open(os.path.join(build_dir, f), mode="w") as file:
                file.writelines(["FILE\n"])
        return build_dir

    @classmethod
    def _get_test_dir_with_archive(cls):
        build_dir = tempfile.mkdtemp()
        tar_dir = cls._get_test_dir()
        with tarfile.open(os.path.join(build_dir, "archive.tar"), mode="w") as tar:
            tar.add(tar_dir)
        shutil.rmtree(tar_dir)
        return build_dir

    @staticmethod
    def _sort_output(output):
        return sorted(output)


if __name__ == '__main__':
    unittest.main()
